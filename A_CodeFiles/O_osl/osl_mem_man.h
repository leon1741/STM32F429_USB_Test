/**************************************************************************************************
**                                                                                               **
**  文件名称:  osl_mem_man.h                                                                     **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  系统动态内存管理模块                                                              **
**  ===========================================================================================  **
**  创建信息:  | 2017-6-29 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef OSL_MEM_MAN_H
#define OSL_MEM_MAN_H

/*************************************************************************************************/
/*                           定义模块变量                                                        */
/*************************************************************************************************/
typedef struct {
    INT32U    ubytes;                                                          /* 已使用的大小 */
    INT32U    lbytes;                                                          /* 未使用的大小 */
    INT32U    blocks;                                                          /* 内存块的数量 */
} HMEM_STAT_T;

/*************************************************************************************************/
/*                           内存状态变化的通知函数                                              */
/*************************************************************************************************/
typedef struct {
    void    (*a_inform)(void *, INT32U, char *, INT32U, HMEM_STAT_T *);        /* 申请内存的结果通知 */
    void    (*f_inform)(void *, INT32U, char *, INT32U, HMEM_STAT_T *);        /* 释放内存的结果通知 */
} MEM_INFORM_T;


void OSL_MemManInit(void);
void *OSL_MemManAlloc(INT32U dlen, char *file, INT32U line, BOOLEAN trac);
void OSL_MemManFree(void *sptr, char *file, INT32U line, BOOLEAN trac);
void OSL_MemManRegA(void (*a_inform)(void *, INT32U, char *, INT32U, HMEM_STAT_T *));
void OSL_MemManRegF(void (*f_inform)(void *, INT32U, char *, INT32U, HMEM_STAT_T *));
HMEM_STAT_T *OSL_MemManGetStat(void);

/*************************************************************************************************/
/*                           系统级宏替代                                                        */
/*************************************************************************************************/
#define SysMem_Alloc(SIZE)                       OSL_MemManAlloc(SIZE, __FILE__, __LINE__, TRUE)
#define SysMem_Free(MPTR)                        OSL_MemManFree(MPTR, __FILE__, __LINE__, TRUE)

#define DbgMem_Alloc(SIZE)                       OSL_MemManAlloc(SIZE, __FILE__, __LINE__, FALSE)
#define DbgMem_Free(MPTR)                        OSL_MemManFree(MPTR, __FILE__, __LINE__, FALSE)

#endif


