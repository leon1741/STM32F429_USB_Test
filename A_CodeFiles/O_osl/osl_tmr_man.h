/**************************************************************************************************
**                                                                                               **
**  文件名称:  osl_tmr_man.h                                                                     **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  系统定时器调度和管理                                                              **
**  ===========================================================================================  **
**  创建信息:  | 2017-6-29 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef OSL_TMR_MAN_H
#define OSL_TMR_MAN_H

/*************************************************************************************************/
/*                           定时精度定义                                                        */
/*************************************************************************************************/
#define PERTICK              1                                                 /* 1 tick = 1 ms */
#define TICK_MILSECOND      (100L / PERTICK)
#define TICK_SECOND         (1000L / PERTICK)
#define TICK_MINUTE         ((60 * 1000L) / PERTICK)

/*************************************************************************************************/
/*                           定时器精度定义                                                      */
/*************************************************************************************************/
#define _TICK                1
#define _MILTICK             TICK_MILSECOND
#define _SECOND              TICK_SECOND
#define _MINUTE              TICK_MINUTE


void OSL_TmrManInit(void);
INT8U OSL_CreateTimer(void (*tmrproc)(void));
void OSL_RemoveTimer(INT8U id);
void OSL_StartTimer(INT8U id, INT32U attrib, INT32U time);
void OSL_StopTimer(INT8U id);
INT32U OSL_GetLeftTime(INT8U id);
INT8U OSL_TimerIsRun(INT8U id);
void OS_ExeTmrFunc(INT8U tskid);
void SystemTimerEntry(void);

#endif


