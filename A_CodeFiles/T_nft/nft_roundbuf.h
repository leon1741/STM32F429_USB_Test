/**************************************************************************************************
**                                                                                               **
**  文件名称:  nft_roundbuf.h                                                                    **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  环形缓冲区功能抽象化                                                              **
**  ===========================================================================================  **
**  创建信息:  | 2017-3-20 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef NFT_ROUNDBUF_H
#define NFT_ROUNDBUF_H

/*************************************************************************************************/
/*                           环形缓冲区结构体定义                                                */
/*************************************************************************************************/
typedef struct {
    INT32U    size;                                                            /* 内存大小 */
    INT32U    used;                                                            /* 已用字节 */
    INT8U    *bptr;                                                            /* 起始地址 */
    INT8U    *eptr;                                                            /* 结束地址 */
    INT8U    *wptr;                                                            /* 读取地址 */
    INT8U    *rptr;                                                            /* 写入地址 */
} ROUNDBUF_T;

void RBUF_InitBuf(ROUNDBUF_T *rbuf, INT8U *mptr, INT32U size);
void RBUF_ClearBuffer(ROUNDBUF_T *rbuf);
INT8U *RBUF_GetStartPtr(ROUNDBUF_T *rbuf);
BOOLEAN RBUF_WriteByte(ROUNDBUF_T *rbuf, INT8U data);
BOOLEAN RBUF_WriteByte_INT(ROUNDBUF_T *rbuf, INT8U data);
BOOLEAN RBUF_WriteBlock(ROUNDBUF_T *rbuf, INT8U *bptr, INT32U len);
BOOLEAN RBUF_WriteBlock_INT(ROUNDBUF_T *rbuf, INT8U *bptr, INT32U len);
INT32S RBUF_ReadByte(ROUNDBUF_T *rbuf);
INT32S RBUF_ReadByte_INT(ROUNDBUF_T *rbuf);
INT32U RBUF_GetLeftByte(ROUNDBUF_T *rbuf);
INT32U RBUF_GetUsedByte(ROUNDBUF_T *rbuf);

#endif

