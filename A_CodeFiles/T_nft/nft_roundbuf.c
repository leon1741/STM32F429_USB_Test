/**************************************************************************************************
**                                                                                               **
**  文件名称:  nft_roundbuf.c                                                                    **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  环形缓冲区功能抽象化                                                              **
**  ===========================================================================================  **
**  创建信息:  | 2017-3-20 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#include "nft_include.h"
#include "nft_roundbuf.h"

#pragma O0

/**************************************************************************************************
**  函数名称:  RBUF_InitBuf
**  功能描述:  初始化循环缓冲区
**  输入参数:  rbuf: 缓冲区地址; mptr: 缓冲区首地址; size: 缓冲区长度
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void RBUF_InitBuf(ROUNDBUF_T *rbuf, INT8U *mptr, INT32U size)
{
    rbuf->size = size;
    rbuf->used = 0;
    rbuf->bptr = mptr;
    rbuf->eptr = mptr + size;
    rbuf->wptr = rbuf->bptr;
    rbuf->rptr = rbuf->bptr;
}

/**************************************************************************************************
**  函数名称:  RBUF_ClearBuffer
**  功能描述:  恢复缓冲区为初始化状态
**  输入参数:  rbuf: 缓冲区地址
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void RBUF_ClearBuffer(ROUNDBUF_T *rbuf)
{
    ENTER_CRITICAL();                            /* 关中断 */
    rbuf->used = 0;
    rbuf->rptr = rbuf->bptr;
    rbuf->wptr = rbuf->bptr;
    EXIT_CRITICAL();                             /* 开中断 */
}

/**************************************************************************************************
**  函数名称:  RBUF_GetStartPtr
**  功能描述:  获取循环缓冲区首地址
**  输入参数:  rbuf: 缓冲区地址
**  输出参数:  无
**  返回参数:  循环缓冲区首地址
**************************************************************************************************/
INT8U *RBUF_GetStartPtr(ROUNDBUF_T *rbuf)
{
    return rbuf->bptr;
}

/**************************************************************************************************
**  函数名称:  RBUF_WriteByte
**  功能描述:  往循环缓冲区中写入一个字节的数据
**  输入参数:  rbuf: 缓冲区地址; indata: 字节数据
**  输出参数:  无
**  返回参数:  成功返回true, 失败返回false
**************************************************************************************************/
BOOLEAN RBUF_WriteByte(ROUNDBUF_T *rbuf, INT8U data)
{
    if (rbuf == 0) {
        return FALSE;
    }
    
    ENTER_CRITICAL();                            /* 关中断 */
    
    if (rbuf->used >= rbuf->size) {
        EXIT_CRITICAL();                         /* 开中断 */
        return FALSE;
    } else {
        EXIT_CRITICAL();                         /* 开中断 */
    }
    
    *rbuf->wptr++ = data;
    
    if (rbuf->wptr >= rbuf->eptr) {
        rbuf->wptr = rbuf->bptr;
    }
    
    ENTER_CRITICAL();                            /* 关中断 */
    rbuf->used++;
    EXIT_CRITICAL();                             /* 开中断 */
    
    return TRUE;
}

/**************************************************************************************************
**  函数名称:  RBUF_WriteByte_INT
**  功能描述:  往循环缓冲区中写入一个字节的数据【无中断保护】
**  输入参数:  rbuf: 缓冲区地址; data: 字节数据
**  输出参数:  无
**  返回参数:  成功返回true, 失败返回false
**************************************************************************************************/
BOOLEAN RBUF_WriteByte_INT(ROUNDBUF_T *rbuf, INT8U data)
{
    if (rbuf == 0) {
        return FALSE;
    }
    
    if (rbuf->used >= rbuf->size) {
        return FALSE;
    }
    
    *rbuf->wptr++ = data;
    if (rbuf->wptr >= rbuf->eptr) {
        rbuf->wptr = rbuf->bptr;
    }

    rbuf->used++;
    return TRUE;
}

/**************************************************************************************************
**  函数名称:  RBUF_WriteBlock
**  功能描述:  写入一串数据到环形缓冲区
**  输入参数:  rbuf: 缓冲区地址; bptr: 数据块的指针; len: 数据块的字节数
**  输出参数:  无
**  返回参数:  成功返回true, 失败返回false
**************************************************************************************************/
BOOLEAN RBUF_WriteBlock(ROUNDBUF_T *rbuf, INT8U *bptr, INT32U len)
{
    INT32U i;
    
    if (len > RBUF_GetLeftByte(rbuf)) {
        return FALSE;
    }
    
    for (i = 0; i < len; i++) {
        *rbuf->wptr++ = *bptr++;
        if (rbuf->wptr >= rbuf->eptr) {
            rbuf->wptr = rbuf->bptr;
        }
        ENTER_CRITICAL();                            /* 关中断 */
        rbuf->used++;
        EXIT_CRITICAL();                             /* 开中断 */
    }
    
    return TRUE;
}

/**************************************************************************************************
**  函数名称:  RBUF_WriteBlock_INT
**  功能描述:  写入一串数据到环形缓冲区【无中断保护】
**  输入参数:  rbuf: 缓冲区地址; bptr: 数据块的指针; len: 数据块的字节数
**  输出参数:  无
**  返回参数:  成功返回true, 失败返回false
**************************************************************************************************/
BOOLEAN RBUF_WriteBlock_INT(ROUNDBUF_T *rbuf, INT8U *bptr, INT32U len)
{
    INT32U i, temp;

    temp = rbuf->size - rbuf->used;
    
    if (len > temp) {
        len = temp;
    }
    
    for (i = 0; i < len; i++) {
        *rbuf->wptr++ = *bptr++;
        if (rbuf->wptr >= rbuf->eptr) {
            rbuf->wptr = rbuf->bptr;
        }
        rbuf->used++;
    }
    
    return TRUE;
}

/**************************************************************************************************
**  函数名称:  RBUF_ReadByte
**  功能描述:  从循环缓冲区中读取一个字节的数据
**  输入参数:  rbuf: 缓冲区地址
**  输出参数:  无
**  返回参数:  成功返回数据值, 失败返回-1
**************************************************************************************************/
INT32S RBUF_ReadByte(ROUNDBUF_T *rbuf)
{
    INT32S ret;
    
    ENTER_CRITICAL();                            /* 关中断 */
    
    if (rbuf->used == 0) {
        EXIT_CRITICAL();                         /* 开中断 */
        return -1;
    } else {
        EXIT_CRITICAL();                         /* 开中断 */
    }
    
    ret = *rbuf->rptr++;
    
    if (rbuf->rptr >= rbuf->eptr) {
        rbuf->rptr = rbuf->bptr;
    }
    
    ENTER_CRITICAL();                            /* 关中断 */
    rbuf->used--;
    EXIT_CRITICAL();                             /* 开中断 */
    
    return ret;
}

/**************************************************************************************************
**  函数名称:  RBUF_ReadByte_INT
**  功能描述:  从循环缓冲区中读取一个字节的数据【无中断保护】
**  输入参数:  rbuf: 缓冲区地址
**  输出参数:  无
**  返回参数:  成功返回数据值, 失败返回-1
**************************************************************************************************/
INT32S RBUF_ReadByte_INT(ROUNDBUF_T *rbuf)
{
    INT32S ret;
    
    if (rbuf->used == 0) {
        return -1;
    }
    
    ret = *rbuf->rptr++;
    
    if (rbuf->rptr >= rbuf->eptr) {
        rbuf->rptr = rbuf->bptr;
    }
    
    rbuf->used--;
    return ret;
}

/**************************************************************************************************
**  函数名称:  RBUF_GetLeftByte
**  功能描述:  获取循环缓冲区中剩余的空间
**  输入参数:  rbuf: 缓冲区地址
**  输出参数:  无
**  返回参数:  剩余空间字节数
**************************************************************************************************/
INT32U RBUF_GetLeftByte(ROUNDBUF_T *rbuf)
{
    INT32U temp;
    
    ENTER_CRITICAL();                            /* 关中断 */
    temp = rbuf->size - rbuf->used;
    EXIT_CRITICAL();                             /* 开中断 */
    
    return temp;
}

/**************************************************************************************************
**  函数名称:  RBUF_GetUsedByte
**  功能描述:  获取循环缓冲区中已使用空间
**  输入参数:  rbuf: 缓冲区地址
**  输出参数:  无
**  返回参数:  已使用空间字节数
**************************************************************************************************/
INT32U RBUF_GetUsedByte(ROUNDBUF_T *rbuf)
{
    INT32U temp;
    
    ENTER_CRITICAL();                            /* 关中断 */
    temp = rbuf->used;
    EXIT_CRITICAL();                             /* 开中断 */
    
    return temp;
}


