/**************************************************************************************************
**                                                                                               **
**  文件名称:  nft_stream.h                                                                      **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  数据流管理功能抽象化                                                              **
**  ===========================================================================================  **
**  创建信息:  | 2017-3-20 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef NFT_STREAM_H
#define NFT_STREAM_H          1

/*************************************************************************************************/
/*                           数据流控制结构体定义                                                */
/*************************************************************************************************/
typedef struct {
    INT8U    *startptr;                                                        /* 起始地址指针 */
    INT8U    *curptr;                                                          /* 当前所处位置指针 */
    INT32U    len;                                                             /* 总长度 */
    INT32U    maxlen;                                                          /* 最大允许长度 */
} STREAM_T;

BOOLEAN STRM_InitStream(STREAM_T *sp, void *bp, INT32U maxlen);
INT32U STRM_GetLeftLen(STREAM_T *sp);
INT32U STRM_GetUsedLen(STREAM_T *sp);
INT32U STRM_GetMaxLen(STREAM_T *sp);
void *STRM_GetCurPtr(STREAM_T *sp);
void *STRM_GetStartPtr(STREAM_T *sp);
void STRM_MovCurPtr(STREAM_T *sp, INT32U len);
void STRM_WriteByte(STREAM_T *sp, INT8U data);
void STRM_WriteHWord_BE(STREAM_T *sp, INT16U data);
void STRM_WriteHWord_LE(STREAM_T *sp, INT16U data);
void STRM_WriteWord_BE(STREAM_T *sp, INT32U data);
void STRM_WriteWord_LE(STREAM_T *sp, INT32U data);
void STRM_WriteString(STREAM_T *sp, char *sptr);
void STRM_WriteBlock(STREAM_T *sp, INT8U *ptr, INT32U len);
INT8U STRM_ReadByte(STREAM_T *sp);
INT16U STRM_ReadHWord_BE(STREAM_T *sp);
INT16U STRM_ReadHWord_LE(STREAM_T *sp);
INT32U STRM_ReadWord_BE(STREAM_T *sp);
INT32U STRM_ReadWord_LE(STREAM_T *sp);
void STRM_ReadBlock(STREAM_T *sp, INT8U *ptr, INT32U len);
STREAM_T *STRM_GetUserBuffer(void);

#endif

