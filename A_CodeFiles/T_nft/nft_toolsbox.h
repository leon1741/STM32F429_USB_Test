/**************************************************************************************************
**                                                                                               **
**  文件名称:  nft_toolsbox.h                                                                    **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  各类工具函数                                                                      **
**  ===========================================================================================  **
**  创建信息:  | 2017-3-20 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef NFT_TOOLSBOX_H
#define NFT_TOOLSBOX_H

#include "sys_struct.h"

/*************************************************************************************************/
/*                           字符比较结果枚举                                                    */
/*************************************************************************************************/
typedef enum {
    STR_EQUAL = 0,                                                             /* 相等 */
    STR_GREAT = 1,                                                             /* 更大 */
    STR_LESS  = 2                                                              /* 更小 */
} CMP_RESULT_E;

/*************************************************************************************************/
/*                           外部宏定义                                                          */
/*************************************************************************************************/
#define CASESENSITIVE        0

void TOOL_SystemDelayms(INT32U msec);
INT8U TOOL_UpperChar(INT8U ch);
INT8U TOOL_LowerChar(INT8U ch);
INT8U TOOL_CompareChar(BOOLEAN matchcase, INT8U ch1, INT8U ch2);
INT8U TOOL_CompareString(BOOLEAN matchcase, INT8U *ptr1, INT8U *ptr2, INT16U len1, INT16U len2);
INT8U TOOL_HexToChar(INT8U inbyte);
INT8U TOOL_CharToHex(INT8U inchar);
INT16U TOOL_Hex8UtoAlg16U(INT8U data);
INT32U TOOL_Hex16UtoAlg32U(INT16U data);
INT8U TOOL_Alg16UtoHex8U(INT16U data);
INT16U TOOL_Alg32UtoHex16U(INT32U data);
INT32U TOOL_AsciiToDec(INT8U *sptr, INT8U len);
INT8U TOOL_DecToAscii(INT8U *dptr, INT32U data, INT8U rlen);
INT8U TOOL_GetChkSum_O(INT8U *dptr, INT32U len);
INT8U TOOL_GetChkSum_N(INT8U *dptr, INT32U len);
INT8U TOOL_GetChkSum_X(INT8U *dptr, INT32U len);
BOOLEAN TOOL_SearchKeyWord(BOOLEAN matchcase, INT8U *dptr, INT16U dlen, char *sptr);
INT16U TOOL_AssembleByRules(INT8U *dptr, INT16U dlen, INT8U *sptr, INT16U slen, ASMRULE_T const *rule);
INT16U TOOL_DeassembleByRules(INT8U *dptr, INT16U dlen, INT8U *sptr, INT16U slen, ASMRULE_T const *rule);
INT32U TOOL_Assemble8Uto32U(INT8U *dptr);
INT8U TOOL_Disassemble32Uto8U(INT8U *dptr, INT32U data);
INT32U TOOL_GetTimeStamp(SYSTIME_T *time);
void TOOL_ConvertTimeStamp(SYSTIME_T *time, INT32U stamp);

#endif

