/**************************************************************************************************
**                                                                                               **
**  文件名称:  nft_list.c                                                                        **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  链表管理功能抽象化                                                                **
**  ===========================================================================================  **
**  创建信息:  | 2017-3-20 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef NFT_LIST_H
#define NFT_LIST_H

/*************************************************************************************************/
/*                           列表节点定义                                                        */
/*************************************************************************************************/
typedef struct listnode {
    struct  listnode  *priv;                                                   /* 前一个节点链接 */
    struct  listnode  *next;                                                   /* 下一个节点链接 */
} NODE_T;

/*************************************************************************************************/
/*                           列表结构体定义                                                      */
/*************************************************************************************************/
typedef struct {
    INT32U     inum;                                                           /* 节点总数目 */
    NODE_T    *head;                                                           /* 头节点地址 */
    NODE_T    *tail;                                                           /* 尾节点地址 */
} LIST_T;

INT32U LIST_GetNodeNum(LIST_T *plist);
INT8U *LIST_GetListHead(LIST_T *plist);
INT8U *LIST_GetListTail(LIST_T *plist);
INT8U *LIST_GetNextEle(INT8U *pnode);
INT8U *LIST_GetPrivEle(INT8U *pnode);
INT8U *LIST_DeleListEle(LIST_T *plist, INT8U *pnode);
INT8U *LIST_DeleListHead(LIST_T *plist);
INT8U *LIST_DeleListTail(LIST_T *plist);
BOOLEAN LIST_AppendListEle(LIST_T *plist, INT8U *pnode);
BOOLEAN LIST_InsertListHead(LIST_T *plist, INT8U *pnode);
BOOLEAN LIST_InsertNodeBefore(LIST_T *plist, INT8U *cnode, INT8U *dnode);
BOOLEAN LIST_InsertNodeAfter(LIST_T *plist, INT8U *cnode, INT8U *dnode);
BOOLEAN LIST_Init(LIST_T *plist);
BOOLEAN LIST_CreateList(LIST_T *plist, INT8U *nodeptr, INT32U nodenum, INT32U nodesize);
BOOLEAN LIST_CheckError(LIST_T *plist, void *bptr, void *eptr);

#endif

