/**************************************************************************************************
**                                                                                               **
**  文件名称:  nft_font.h                                                                        **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  字体抽象化管理模块                                                                **
**  ===========================================================================================  **
**  创建信息:  | 2018-7-09 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef NFT_FONTS_H
#define NFT_FONTS_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>

typedef struct _tFont
{    
  const uint16_t *table;
  uint16_t Width;
  uint16_t Height;
} sFONT;

extern sFONT Font16x24;
extern sFONT Font12x12;
extern sFONT Font8x12;
extern sFONT Font8x8;

#ifdef __cplusplus
}
#endif
  
#endif


