/**************************************************************************************************
**                                                                                               **
**  文件名称:  bsp_led_drv.h                                                                      **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  LED闪灯注册及驱动模块                                                             **
**  ===========================================================================================  **
**  创建信息:  | 2017-4-16 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef BSP_LED_DRV_H
#define BSP_LED_DRV_H

/*************************************************************************************************/
/*                           LED名称枚举定义                                                     */
/*************************************************************************************************/
#ifdef LEDDRV_DEF
#undef LEDDRV_DEF
#endif

#define LEDDRV_DEF(_LED_NAME_, _LED_PORT_, _LED_PIN_, _LED_DIRT_, _LED_INIT_)  _LED_NAME_,

typedef enum {
    #include "bsp_led_drv.def"
    LED_NAME_MAX
} LED_NAME_E;

void BSP_LEDDrvInit(void);
void BSP_ReverseLEDPort(LED_NAME_E name);
void BSP_LightupLEDPort(LED_NAME_E name);
void BSP_TurnOFFLEDPort(LED_NAME_E name);

#endif

