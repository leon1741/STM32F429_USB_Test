/**************************************************************************************************
**                                                                                               **
**  文件名称:  bsp_led_drv.c                                                                     **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  LED闪灯注册及驱动模块                                                             **
**  ===========================================================================================  **
**  创建信息:  | 2017-4-16 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#include "bsp_include.h"
#include "bsp_led_drv.h"

/*************************************************************************************************/
/*                           LED驱动控制结构体                                                   */
/*************************************************************************************************/
typedef struct {
    LED_NAME_E      name;                                                      /* LED名称 */
    GPIO_TypeDef*   port;                                                      /* 端口号 */
    INT32U          pin;                                                       /* 管脚号 */
    INT8U           dirt;                                                      /* 上拉点亮还是下拉点亮 */
    INT8U           init;                                                      /* 初始值 */
} LED_CTRL_T;

#ifdef LEDDRV_DEF
#undef LEDDRV_DEF
#endif

#define LEDDRV_DEF(_LED_NAME_, _LED_PORT_, _LED_PIN_, _LED_DIRT_, _LED_INIT_)  \
                  {_LED_NAME_, _LED_PORT_, _LED_PIN_, _LED_DIRT_, _LED_INIT_},

static const LED_CTRL_T s_leddrv_tbl[] = {
    #include "bsp_led_drv.def"
    {LED_NAME_MAX, GPIOA, 0, 0, 0}
};

/**************************************************************************************************
**  函数名称:  LED_GetRegInfo
**  功能描述:  获取注册信息
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static LED_CTRL_T const *LED_GetRegInfo(LED_NAME_E name)
{
    return &s_leddrv_tbl[name];
}

/**************************************************************************************************
**  函数名称:  BSP_LEDDrvInit
**  功能描述:  LED闪灯驱动模块初始化
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void BSP_LEDDrvInit(void)
{
    LED_NAME_E        name;
    LED_CTRL_T const *ctrl;
    GPIO_InitTypeDef  gpio;
    
    for (name = (LED_NAME_E)0; name < LED_NAME_MAX; name ++) {                 /* 依次初始化各个LED */
        
        ctrl = LED_GetRegInfo(name);
        
        gpio.GPIO_Pin   = ctrl->pin;
        gpio.GPIO_Mode  = GPIO_Mode_OUT;
        gpio.GPIO_OType = GPIO_OType_PP;
        gpio.GPIO_Speed = GPIO_Speed_50MHz;
        gpio.GPIO_PuPd  = GPIO_PuPd_UP;
        
        GPIO_Init(ctrl->port, &gpio);
        
        if (ctrl->init == 1) {                                                 /* 依次设置初始电平 */
            BSP_LightupLEDPort(name);
        } else {
            BSP_TurnOFFLEDPort(name);
        }
    }
}

/**************************************************************************************************
**  函数名称:  BSP_ReverseLEDPort
**  功能描述:  翻转LED闪烁状态
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void BSP_ReverseLEDPort(LED_NAME_E name)
{
    LED_CTRL_T const *ctrl;

    if (name >= LED_NAME_MAX) {                                                /* 超过允许范围 */
        return;
    }
    
    ctrl = LED_GetRegInfo(name);
    
    if (GPIO_ReadOutputDataBit(ctrl->port, ctrl->pin) == Bit_SET) {
        GPIO_ResetBits(ctrl->port, ctrl->pin);
    } else {
        GPIO_SetBits(ctrl->port, ctrl->pin);
    }
}

/**************************************************************************************************
**  函数名称:  BSP_LightupLEDPort
**  功能描述:  打开LED灯
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void BSP_LightupLEDPort(LED_NAME_E name)
{
    LED_CTRL_T const *ctrl;
    
    if (name >= LED_NAME_MAX) {                                                /* 超过允许范围 */
        return;
    }
    
    ctrl = LED_GetRegInfo(name);

    if (ctrl->dirt == TRUE) {
        GPIO_SetBits(ctrl->port, ctrl->pin);
    } else {
        GPIO_ResetBits(ctrl->port, ctrl->pin);
    }
}

/**************************************************************************************************
**  函数名称:  BSP_TurnOFFLEDPort
**  功能描述:  关闭LED灯
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void BSP_TurnOFFLEDPort(LED_NAME_E name)
{
    LED_CTRL_T const *ctrl;
    
    if (name >= LED_NAME_MAX) {                                                /* 超过允许范围 */
        return;
    }
    
    ctrl = LED_GetRegInfo(name);
    
    if (ctrl->dirt == TRUE) {
        GPIO_ResetBits(ctrl->port, ctrl->pin);
    } else {
        GPIO_SetBits(ctrl->port, ctrl->pin);
    }
}


