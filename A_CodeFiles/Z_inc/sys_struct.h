/**************************************************************************************************
**                                                                                               **
**  文件名称:  sys_struct.h                                                                       **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  系统通用数据结构定义                                                              **
**  ===========================================================================================  **
**  创建信息:  | 2017-3-16 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef SYS_STRUCT_H
#define SYS_STRUCT_H

#include "sys_type.h"
#include "sys_config.h"

/*************************************************************************************************/
/*                           联合体定义                                                          */
/*************************************************************************************************/
typedef union {
	INT16U     hword;
#ifdef __BIG_ENDIAN
	struct {
		INT8U  high;
		INT8U  low;
	} bytes;
#else
	struct {
		INT8U  low;
		INT8U  high;
	} bytes;
#endif
} HALFWORD_UNION;                                                              /* 半字联合体 */

typedef union {
    INT32U ulong;
#ifdef  __BIG_ENDIAN   
    struct {
		INT8U  byte1;
		INT8U  byte2;
        INT8U  byte3;
		INT8U  byte4;
	} bytes;
#else
    struct {
		INT8U  byte4;
		INT8U  byte3;
        INT8U  byte2;
		INT8U  byte1;
	} bytes;
#endif
} LONGWORD_UNION;                                                              /* 全字联合体 */

/*************************************************************************************************/
/*                           定义日期时间                                                        */
/*************************************************************************************************/
typedef struct {
    INT8U year;
    INT8U month;
    INT8U day;
} DATE_T;

typedef struct {
    INT8U hour;
    INT8U minute;
    INT8U second;
} TIME_T;

typedef struct {
    DATE_T date;
    TIME_T time;
} SYSTIME_T;

/*************************************************************************************************/
/*                           处理函数结构                                                        */
/*************************************************************************************************/
typedef struct {
    INT32U   index;
    void   (*entryproc)(void);
} FUNCTION_T;

typedef struct {
    INT32U   index;
    void   (*entryproc)(INT32U type, INT8U *data, INT16U datalen);
} FUNCTION_PARA_T;

/*************************************************************************************************/
/*                           协议帧结构定义                                                      */
/*************************************************************************************************/
typedef struct {
    INT8U       c_flags;                                   /* c_convert0 + c_convert1 = c_flags */
    INT8U       c_convert0;                                /* c_convert0 + c_convert2 = c_convert0 */
    INT8U       c_convert1;
    INT8U       c_convert2;
} ASMRULE_T;

#endif


