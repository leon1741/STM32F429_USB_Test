/**************************************************************************************************
**                                                                                               **
**  文件名称:  sys_type.h                                                                         **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  系统数据类型定义                                                                  **
**  ===========================================================================================  **
**  创建信息:  | 2017-3-16 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef SYS_TYPE_H
#define SYS_TYPE_H

/*************************************************************************************************/
/*                           系统数据类型定义                                                    */
/*************************************************************************************************/
typedef unsigned char        BOOLEAN;
typedef unsigned char        INT8U;                                            /* Unsigned  8 bit quantity */
typedef signed   char        INT8S;                                            /* Signed    8 bit quantity */
typedef unsigned short       INT16U;                                           /* Signed   16 bit quantity */
typedef signed   short       INT16S;                                           /* Unsigned 32 bit quantity */
typedef unsigned int         INT32U;                                           /* Unsigned 32 bit quantity */
typedef unsigned long long   INT64U;                                           /* Unsigned 64 bit quantity */
typedef signed   int         INT32S;                                           /* Signed   32 bit quantity */
typedef float                FP32;                                             /* Single precision floating point */
typedef double               FP64;                                             /* Double precision floating point */

/*************************************************************************************************/
/*                           定义关键字                                                          */
/*************************************************************************************************/
#ifndef false
#define false                0
#endif

#ifndef true
#define true                 1
#endif

#ifndef TRUE
#define TRUE                 1
#endif

#ifndef FALSE
#define FALSE                0
#endif

#ifndef NULL 
#define NULL                 ((void *)0)
#endif

#ifndef RETURN_VOID
#define RETURN_VOID
#endif

#ifndef RETURN_TRUE
#define RETURN_TRUE          TRUE
#endif

#ifndef RETURN_FALSE
#define RETURN_FALSE         FALSE
#endif

#ifndef	 _SUCCESS
#define  _SUCCESS            0
#endif

#ifndef	 _FAILURE
#define  _FAILURE            1
#endif

#ifndef  _OVERTIME
#define  _OVERTIME           2
#endif

#endif


