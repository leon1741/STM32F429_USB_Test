/**************************************************************************************************
**                                                                                               **
**  文件名称:  sys_include.h                                                                      **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  系统常用头文件包含                                                                **
**  ===========================================================================================  **
**  创建信息:  | 2017-3-16 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef SYS_INCLUDE_H
#define SYS_INCLUDE_H

#include <string.h>
#include <stdarg.h>
#include <stdio.h>

#include "sys_type.h"
#include "sys_flash.h"
#include "sys_struct.h"
#include "sys_config.h"

#include "stm32f4xx.h"
#include "core_cm4.h"
#include "core_cmFunc.h"

/*************************************************************************************************/
/*                           各个系统函数的统一定义                                              */
/*************************************************************************************************/
#define YX_MEMSET            memset
#define YX_VSPRINTF          vsprintf
#define YX_STRLEN            strlen
#define YX_STRCPY            strcpy
#define YX_STRCAT            strcat
#define YX_MEMCMP            memcmp
#define YX_MEMCPY            memcpy
#define YX_VA_START          va_start
#define YX_VA_END            va_end

#define ENTER_CRITICAL()     do {                                    \
                                   if (!(__get_IPSR() & 0x1ff)) {    \
                                      __disable_fault_irq();         \
                                   }                                 \
                                } while(0)


#define EXIT_CRITICAL()      do {                                    \
                                   if (!(__get_IPSR() & 0x1ff)) {    \
                                      __enable_fault_irq();          \
                                   }                                 \
                                } while(0)
#endif


