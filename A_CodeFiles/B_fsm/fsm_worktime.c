/**************************************************************************************************
**                                                                                               **
**  文件名称:  fsm_worktime.c                                                                    **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  工作时间统计模块                                                                  **
**  ===========================================================================================  **
**  创建信息:  | 2017-4-14 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#include "fsm_include.h"
#include "fsm_worktime.h"

/*************************************************************************************************/
/*                           模块宏                                                              */
/*************************************************************************************************/
#define WORKTIME_SCAN        _MINUTE, 1

/*************************************************************************************************/
/*                           模块静态变量定义                                                    */
/*************************************************************************************************/
static INT8U          s_worktimetmr;
static WORKTIME_T     s_worktimerec;                                           /* 工作小时计数器 */

/**************************************************************************************************
**  函数名称:  WorktimeScanHdl
**  功能描述:  模块定时器处理函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void WorktimeScanHdl(void)
{
    PP_ReadItem(PP_WORKTIME, &s_worktimerec);
    
    s_worktimerec.thistime++;                                                  /* 本次工作时长累加 */
    s_worktimerec.totltime++;                                                  /* 总工作时长累加 */
    
    #if DEBUG_COMM > 0
    Debug_SysPrint("Worktime Update: today(%02d hour %02d min). total(%02d hour %02d min)\n", 
                   s_worktimerec.thistime / 60, s_worktimerec.thistime % 60, s_worktimerec.totltime / 60, s_worktimerec.totltime % 60);
    #endif
    
    PP_WriteItem(PP_WORKTIME, &s_worktimerec, FALSE);
}

/**************************************************************************************************
**  函数名称:  Worktime_Init
**  功能描述:  模块初始化函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void Worktime_Init(void)
{
    YX_MEMSET(&s_worktimerec, 0, sizeof(s_worktimerec));
    
    s_worktimetmr = OSL_CreateTimer(WorktimeScanHdl);
    OSL_StartTimer(s_worktimetmr, WORKTIME_SCAN);
    
    if (PP_ReadItem(PP_WORKTIME, &s_worktimerec) == FALSE) {
        YX_MEMSET(&s_worktimerec, 0, sizeof(s_worktimerec));
    }
    
    s_worktimerec.lasttime = s_worktimerec.thistime;                           /* 更新上次工作的时间值 */
    s_worktimerec.thistime = 0;
    PP_WriteItem(PP_WORKTIME, &s_worktimerec, TRUE);
    
    #if DEBUG_COMM > 0
    Debug_SysPrint("Total Worktime: last_time(%02d hour %02d min). total_time(%02d hour %02d min)\n", 
                   s_worktimerec.lasttime / 60, s_worktimerec.lasttime % 60, s_worktimerec.totltime / 60, s_worktimerec.totltime % 60);
    #endif
}

/**************************************************************************************************
**  函数名称:  GetSysLastWktime
**  功能描述:  获取上次工作的总时长【单位: 分钟】
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
INT32U GetSysLastWktime(void)
{
    return s_worktimerec.lasttime;
}

/**************************************************************************************************
**  函数名称:  GetSysThisWktime
**  功能描述:  获取本次开机后的工作总时长【单位: 分钟】
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
INT32U GetSysThisWktime(void)
{
    return s_worktimerec.thistime;
}

/**************************************************************************************************
**  函数名称:  GetSysTotalWktime
**  功能描述:  获取设备出厂后的工作总时长【单位: 分钟】
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
INT32U GetSysTotalWktime(void)
{
    return s_worktimerec.totltime;
}


