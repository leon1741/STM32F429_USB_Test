/**************************************************************************************************
**                                                                                               **
**  文件名称:  fsm_admin.c                                                                       **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  FAL层入口及管控模块                                                               **
**  ===========================================================================================  **
**  创建信息:  | 2017-6-30 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#include "fsm_include.h"
#include "fsm_worktime.h"
#include "fsm_resetrec.h"
#include "fsm_pwrcheck.h"

/**************************************************************************************************
**  函数名称:  FSMAdmin_Initiate
**  功能描述:  FSM层管控模块初始化
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void FSMAdmin_Initiate(void)
{
    //Worktime_Init();
    //ResetRec_Init();
    //PwrCheck_Init();
}


