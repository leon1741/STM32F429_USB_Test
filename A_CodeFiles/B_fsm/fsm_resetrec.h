/**************************************************************************************************
**                                                                                               **
**  文件名称:  fsm_resetrec.h                                                                    **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  复位管理模块                                                                      **
**  ===========================================================================================  **
**  创建信息:  | 2017-4-24 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef FSM_RESETREC_H
#define FSM_RESETREC_H

void ResetRec_Init(void);
void PrintSysResetRec(void);
void ClearAllResetRec(void);
INT32U GetIntResetCnt(void);
INT32U GetExtResetCnt(void);
INT32U GetTotalResetCnt(void);

#endif

