/**************************************************************************************************
**                                                                                               **
**  文件名称:  app_lcd_test.c                                                                    **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  LCD显示屏及触摸屏测试模块                                                         **
**  ===========================================================================================  **
**  创建信息:  | 2018-7-9 | LEON | 创建本模块                                                    **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#include "app_include.h"
#include "app_lcd_test.h"

/*************************************************************************************************/
/*                           模块宏定义                                                          */
/*************************************************************************************************/
#define PERIOD_TLCD_1T       _TICK, 10
#define PERIOD_TLCD_1S       _SECOND, 1
#define PERIOD_TLCD_1M       _MINUTE, 1

/*************************************************************************************************/
//                           区域结构体定义
/*************************************************************************************************/
typedef struct {
    INT16U    xpos;                                                            /* X 坐标点 */
    INT16U    ypos;                                                            /* Y 坐标点 */
    INT16U    width;                                                           /* 区域宽度 */
    INT16U    height;                                                          /* 区域高度 */
} AREA_T;

/*************************************************************************************************/
/*                           定义模块变量                                                        */
/*************************************************************************************************/
static INT8U          s_tlcdtmr_1t, s_tlcdtmr_1s, s_tlcdtmr_1m;
static INT16U         s_curt_color;                                            /* 当前颜色 */

static AREA_T         s_blue   = {  0, 280,  40,  40};
static AREA_T         s_red    = { 40, 280,  40,  40};
static AREA_T         s_green  = { 80, 280,  40,  40};
static AREA_T         s_black  = {120, 280,  40,  40};
static AREA_T         s_magent = {160, 280,  40,  40};
static AREA_T         s_cyan   = {200, 280,  40,  40};
static AREA_T         s_valid  = {  5,   5, 230, 265};

/**************************************************************************************************
**  函数名称:  IsInRectangle
**  功能描述:  判断一个点是否落在矩形区域内
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static BOOLEAN IsInRectangle(TP_STATE *pos, AREA_T *area)
{
    if (pos->X < area->xpos) {
        return FALSE;
    }

    if (pos->X > (area->xpos + area->width)) {
        return FALSE;
    }

    if (pos->Y < area->ypos) {
        return FALSE;
    }

    if (pos->Y > (area->ypos + area->height)) {
        return FALSE;
    }
    
    return TRUE;
}

/**************************************************************************************************
**  函数名称:  TouchPannelTest
**  功能描述:  触摸屏及LCD显示屏测试函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void TouchPannelTest(void)
{
    TP_STATE *TP_State;
    
    TP_State = IOE_TP_GetState();
    
    if (TP_State->TouchDetected == FALSE) {
        return;
    }
    
    if (IsInRectangle(TP_State, &s_blue) == TRUE) {
        if (s_curt_color != LCD_COLOR_BLUE) {
            #if DEBUG_COMM > 0
            Debug_SysPrint("blue is set\r\n");
            #endif
            DAL_LCD_SetTextColor(LCD_COLOR_BLUE);
            s_curt_color = LCD_COLOR_BLUE;
        }
        return;
    }
    
    if (IsInRectangle(TP_State, &s_red) == TRUE) {
        if (s_curt_color != LCD_COLOR_RED) {
            #if DEBUG_COMM > 0
            Debug_SysPrint("red is set\r\n");
            #endif
            DAL_LCD_SetTextColor(LCD_COLOR_RED);
            s_curt_color = LCD_COLOR_RED;
        }
        return;
    }
    
    if (IsInRectangle(TP_State, &s_green) == TRUE) {
        if (s_curt_color != LCD_COLOR_GREEN) {
            #if DEBUG_COMM > 0
            Debug_SysPrint("green is set\r\n");
            #endif
            DAL_LCD_SetTextColor(LCD_COLOR_GREEN);
            s_curt_color = LCD_COLOR_GREEN;
        }
        return;
    }
    
    if (IsInRectangle(TP_State, &s_black) == TRUE) {
        if (s_curt_color != LCD_COLOR_BLACK) {
            #if DEBUG_COMM > 0
            Debug_SysPrint("black is set\r\n");
            #endif
            DAL_LCD_SetTextColor(LCD_COLOR_BLACK);
            s_curt_color = LCD_COLOR_BLACK;
        }
        return;
    }
    
    if (IsInRectangle(TP_State, &s_magent) == TRUE) {
        if (s_curt_color != LCD_COLOR_MAGENTA) {
            #if DEBUG_COMM > 0
            Debug_SysPrint("magenta is set\r\n");
            #endif
            DAL_LCD_SetTextColor(LCD_COLOR_MAGENTA);
            s_curt_color = LCD_COLOR_MAGENTA;
        }
        return;
    }
    
    if (IsInRectangle(TP_State, &s_cyan) == TRUE) {
        if (s_curt_color != LCD_COLOR_CYAN) {
            #if DEBUG_COMM > 0
            Debug_SysPrint("cyan is set\r\n");
            #endif
            DAL_LCD_SetTextColor(LCD_COLOR_CYAN);
            s_curt_color = LCD_COLOR_CYAN;
        }
        return;
    }
    
    if (IsInRectangle(TP_State, &s_valid) == TRUE) {
        DAL_LCD_DrawFullCircle(TP_State->X, TP_State->Y, 3);
    } else {
        ;
    }
}

/**************************************************************************************************
**  函数名称:  LCDDisplay_Init
**  功能描述:  LCD显示屏初始化
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void LCDDisplay_Init(void)
{
    s_curt_color = LCD_COLOR_BLACK;

    DAL_LCD_Clear(LCD_COLOR_WHITE);

    if (IOE_Config() == IOE_OK) {
        DAL_LCD_SetTextColor(LCD_COLOR_BLUE); 
        DAL_LCD_DrawFullRect(s_blue.xpos, s_blue.ypos, s_blue.width, s_blue.height);
        DAL_LCD_SetTextColor(LCD_COLOR_RED); 
        DAL_LCD_DrawFullRect(s_red.xpos, s_red.ypos, s_red.width, s_red.height);
        DAL_LCD_SetTextColor(LCD_COLOR_GREEN); 
        DAL_LCD_DrawFullRect(s_green.xpos, s_green.ypos, s_green.width, s_green.height);
        DAL_LCD_SetTextColor(LCD_COLOR_BLACK); 
        DAL_LCD_DrawFullRect(s_black.xpos, s_black.ypos, s_black.width, s_black.height);
        DAL_LCD_SetTextColor(LCD_COLOR_MAGENTA); 
        DAL_LCD_DrawFullRect(s_magent.xpos, s_magent.ypos, s_magent.width, s_magent.height);
        DAL_LCD_SetTextColor(LCD_COLOR_CYAN); 
        DAL_LCD_DrawFullRect(s_cyan.xpos, s_cyan.ypos, s_cyan.width, s_cyan.height);
        DAL_LCD_SetTextColor(s_curt_color);
    } else {
        DAL_LCD_Clear(LCD_COLOR_RED);
        DAL_LCD_SetTextColor(s_curt_color); 
        DAL_LCD_DisplayStringLine(LCD_LINE_6, (INT8U *)"   IOE NOT OK     ");
        DAL_LCD_DisplayStringLine(LCD_LINE_7, (INT8U *)"Reset the board   ");
        DAL_LCD_DisplayStringLine(LCD_LINE_8, (INT8U *)"and try again     ");
    }
}

/**************************************************************************************************
**  函数名称:  LCDTestTmrProc_1t
**  功能描述:  1 tick 定时器
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void LCDTestTmrProc_1t(void)
{
    TouchPannelTest();
}

/**************************************************************************************************
**  函数名称:  LCDTestTmrProc_1s
**  功能描述:  1 second 定时器
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void LCDTestTmrProc_1s(void)
{
    
}

/**************************************************************************************************
**  函数名称:  LCDTestTmrProc_1m
**  功能描述:  1 minute 定时器
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void LCDTestTmrProc_1m(void)
{
    
}

/**************************************************************************************************
**  函数名称:  LCDKeyReleaseHdl
**  功能描述:  用户按键松开处理函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void LCDKeyReleaseHdl(INPUT_PORT_E port, INPUT_TRIG_E trigmode)
{
    LCDDisplay_Init();
}

/**************************************************************************************************
**  函数名称:  APP_LCDTestInit
**  功能描述:  模块初始化函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void APP_LCDTestInit(void)
{
    LCDDisplay_Init();

    s_tlcdtmr_1t = OSL_CreateTimer(LCDTestTmrProc_1t);
    OSL_StartTimer(s_tlcdtmr_1t, PERIOD_TLCD_1T);
    
    s_tlcdtmr_1s = OSL_CreateTimer(LCDTestTmrProc_1s);
    OSL_StartTimer(s_tlcdtmr_1s, PERIOD_TLCD_1S);
    
    s_tlcdtmr_1m = OSL_CreateTimer(LCDTestTmrProc_1m);
    OSL_StartTimer(s_tlcdtmr_1m, PERIOD_TLCD_1M);

    DAL_INPUT_InstallTriggerProc(INPUT_USRKEY, INPUT_TRIG_NEGATIVE, LCDKeyReleaseHdl);
}


