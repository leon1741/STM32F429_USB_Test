/**************************************************************************************************
**                                                                                               **
**  文件名称:  app_main.c                                                                        **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  主程序入口函数main调用                                                            **
**  ===========================================================================================  **
**  创建信息:  | 2017-7-3 | LEON | 创建本模块                                                    **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#pragma import(__use_no_heap_region)
#include "app_main.h"
#include "app_admin.h"
#include "fsm_admin.h"
#include "usb_admin.h"
#include "dal_admin.h"
#include "osl_admin.h"
#include "bsp_admin.h"
#include "nft_admin.h"

/**************************************************************************************************
**  函数名称:  Hardware_Initiate
**  功能描述:  系统板级硬件初始化
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void Hardware_Initiate(void)
{
    //NVIC_SetVectorTable(NVIC_VectTab_RAM, 0);
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
    
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD |
                           RCC_AHB1Periph_GPIOE | RCC_AHB1Periph_GPIOF | RCC_AHB1Periph_GPIOG | RCC_AHB1Periph_GPIOH |
                           RCC_AHB1Periph_GPIOI | RCC_AHB1Periph_GPIOJ | RCC_AHB1Periph_GPIOK, ENABLE);
}

/**************************************************************************************************
**  函数名称:  main
**  功能描述:  系统主函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
int main(void)
{
    Hardware_Initiate();                                                       /* 系统硬件入口初始化 */
    USBAdmin_Initiate();                                                       /* USB 驱动入口初始化 */
    NFTAdmin_Initiate();                                                       /* 功能无关集合初始化 */
    BSPAdmin_Initiate();                                                       /* 板级驱动集合初始化 */
    OSLAdmin_Initiate();                                                       /* 操作系统接口初始化 */
    DALAdmin_Initiate();                                                       /* 设备抽象模块初始化 */
    FSMAdmin_Initiate();                                                       /* 功能抽象模块初始化 */
    APPAdmin_Initiate();                                                       /* 功能应用模块初始化 */
    
    for (;;) {
        SystemTimerEntry();
        SysMsgSchedEntry();
        USBMessageHandle();
    }
}


