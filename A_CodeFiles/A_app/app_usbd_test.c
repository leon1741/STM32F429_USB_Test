/**************************************************************************************************
**                                                                                               **
**  文件名称:  app_usbh_test.c                                                                    **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  USB文件系统测试模块                                                               **
**  ===========================================================================================  **
**  创建信息:  | 2018-7-19 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#include "app_include.h"
#include "usbd_bsp.h"
#include "app_usbd_test.h"

#ifdef USB_DEV

/*************************************************************************************************/
/*                           模块宏定义                                                          */
/*************************************************************************************************/
#define PERIOD_USBDTEST      _SECOND, 1

/*************************************************************************************************/
/*                           定义模块变量                                                        */
/*************************************************************************************************/
static INT8U          s_usbdtesttmr;

#if 0
/**************************************************************************************************
**  函数名称:  USBH_TestStatInform
**  功能描述:  USB设备状态通知回调函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void USBH_TestStatInform(USBH_STAT_E stat)
{
    switch (stat)
    {
        case USBH_STAT_INIT:
            #if DEBUG_COMM > 0
            Debug_SysPrint("USB_Host Init...\r\n");
            #endif
            break;
            
        case USBH_STAT_DEINIT:
            #if DEBUG_COMM > 0
            Debug_SysPrint("USB_Host Deinit...\r\n");
            #endif
            break;
            
        case USBH_STAT_ATTACHED:
            #if DEBUG_COMM > 0
            Debug_SysPrint("USB_Host Attached...\r\n");
            #endif
            break;
            
        case USBH_STAT_RESETDEVICE:
            #if DEBUG_COMM > 0
            Debug_SysPrint("USB_Host Reset Device...\r\n");
            #endif
            break;
            
        case USBH_STAT_OVERCURRENT:
            #if DEBUG_COMM > 0
            Debug_SysPrint("USB_Host Over Current...\r\n");
            #endif
            break;
            
        case USBH_STAT_DISCONNECTED:
            #if DEBUG_COMM > 0
            Debug_SysPrint("USB_Host Disconnected...\r\n");
            #endif
            s_usbhfsinitd = FALSE;
            s_usbhfsready = FALSE;
            f_mount(NULL, NULL, 0);
            OSL_PostSysMsg(MSG_USB_NOTINST, 0, 0, FALSE);
            break;
                        
        case USBH_STAT_ENUMERATIONDONE:
            #if DEBUG_COMM > 0
            Debug_SysPrint("USB_Host Enum Done...\r\n");
            #endif
            s_usbhfsinitd = FALSE;
            s_usbhfsready = FALSE;
            OSL_PostSysMsg(MSG_USB_DEVVALD, 0, 0, FALSE);
            break;
            
        case USBH_STAT_MSC_APPLICATION:
            #if DEBUG_COMM > 0
            Debug_SysPrint("USB_Host Application...\r\n");
            #endif
            if (s_usbhfsinitd == FALSE) {
                s_usbhfsinitd = TRUE;
                CheckFileSystem();
            }
            break;
            
        case USBH_STAT_NOTSUPPORTED:
            #if DEBUG_COMM > 0
            Debug_SysPrint("USB_Host NOT Surpported...\r\n");
            #endif
            OSL_PostSysMsg(MSG_USB_DEVEROR, 0, 0, FALSE);
            break;
            
        case USBH_STAT_UNRECOVEREDERROR:
            #if DEBUG_COMM > 0
            Debug_SysPrint("USB_Host Error...\r\n");
            #endif
            OSL_PostSysMsg(MSG_USB_DEVEROR, 0, 0, FALSE);
            break;

        default:
            #if DEBUG_COMM > 0
            Debug_SysPrint("USBH_TestStatInform: Stat Error!!!\r\n");
            #endif
            break;
    }
}
#endif

static int s_count;

/**************************************************************************************************
**  函数名称:  USBD_TestTmrProc
**  功能描述:  定时器
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void USBD_TestTmrProc(void)
{
    Debug_SysPrint("USBD_TestTmrProc...\r\n");
    USB_SendData(0, s_count++);
}

/**************************************************************************************************
**  函数名称:  APP_UsbdTestInit
**  功能描述:  模块初始化函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void APP_UsbdTestInit(void)
{
    s_usbdtesttmr = OSL_CreateTimer(USBD_TestTmrProc);
    OSL_StartTimer(s_usbdtesttmr, PERIOD_USBDTEST);
    
    //USBH_USR_RegStatHdl(&USBH_TestStatInform);                                 /* 注册回调函数 */
}

#endif


