/**************************************************************************************************
**                                                                                               **
**  文件名称:  app_lcd_test.h                                                                    **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  LCD显示屏及触摸屏测试模块                                                         **
**  ===========================================================================================  **
**  创建信息:  | 2018-7-9 | LEON | 创建本模块                                                    **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef APP_LCD_TEST_H
#define APP_LCD_TEST_H

void APP_LCDTestInit(void);

#endif


