/**************************************************************************************************
**                                                                                               **
**  文件名称:  app_usr_test.c                                                                    **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  用户临时测试模块                                                                  **
**  ===========================================================================================  **
**  创建信息:  | 2017-8-2 | LEON | 创建本模块                                                    **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#include "app_include.h"
#include "app_usr_test.h"

/*************************************************************************************************/
/*                           模块宏定义                                                          */
/*************************************************************************************************/
#define PERIOD_TEST_1T       _TICK, 1
#define PERIOD_TEST_1S       _SECOND, 1
#define PERIOD_TEST_1M       _MINUTE, 1

#define ETEST_HIGHTIME       0                                                 /* 测试高精度定时器 */

/*************************************************************************************************/
/*                           定义模块变量                                                        */
/*************************************************************************************************/
static INT8U          s_testtmr_1t, s_testtmr_1s, s_testtmr_1m;

#if ETEST_HIGHTIME
static INT32U         s_systickcnt;
static INT32U         s_systickpre, s_systickcur, s_systickdif;
#endif

/**************************************************************************************************
**  函数名称:  TestTmrProc_1t
**  功能描述:  1 tick 定时器
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void TestTmrProc_1t(void)
{
    #if ETEST_HIGHTIME
    s_systickcnt++;
    
    #if DEBUG_COMM > 0
    Debug_SysPrint("%.8d\r\n", s_systickcnt);
    #endif
    #endif
}

/**************************************************************************************************
**  函数名称:  TestTmrProc_1s
**  功能描述:  1 second 定时器
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void TestTmrProc_1s(void)
{
    #if ETEST_HIGHTIME
    s_systickdif = s_systickcnt - s_systickpre;
    s_systickpre = s_systickcur;
    s_systickcur = s_systickcnt;
    
    #if DEBUG_COMM > 0
    Debug_SysPrint("systickcur %.8d. systickdif %.8d...\r\n", s_systickcur, s_systickdif);
    #endif
    #endif
}

/**************************************************************************************************
**  函数名称:  TestTmrProc_1m
**  功能描述:  1 minute 定时器
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void TestTmrProc_1m(void)
{
    #if DEBUG_COMM > 5
    Debug_SysPrint("1 minute has passed...\r\n");
    #endif
}

/**************************************************************************************************
**  函数名称:  MemAllcHdl
**  功能描述:  申请内存的结果通知
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void MemAllcHdl(void *aptr, INT32U mlen, char *file, INT32U line, HMEM_STAT_T *stat)
{
    #if DEBUG_COMM > 9
    Debug_SysPrint("Allc: mptr: 0x%08X. mlen: %04d. file: %15s. line: %04d. ", aptr, mlen, file, line);
    Debug_SysPrint("[Stat: used: %04d. left: %04d. block: %04d]\r\n", stat->ubytes, stat->lbytes, stat->blocks);
    #endif
}

/**************************************************************************************************
**  函数名称:  MemFreeHdl
**  功能描述:  释放内存的结果通知
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void MemFreeHdl(void *aptr, INT32U mlen, char *file, INT32U line, HMEM_STAT_T *stat)
{
    #if DEBUG_COMM > 9
    Debug_SysPrint("Free: mptr: 0x%08X. mlen: %04d. file: %15s. line: %04d. ", aptr, mlen, file, line);
    Debug_SysPrint("[Stat: used: %04d. left: %04d. block: %04d]\r\n", stat->ubytes, stat->lbytes, stat->blocks);
    #endif
}

/**************************************************************************************************
**  函数名称:  UserKeyPressedHdl
**  功能描述:  用户按键按下处理函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void UserKeyPressedHdl(INPUT_PORT_E port, INPUT_TRIG_E trigmode)
{
    #if DEBUG_COMM > 0
    Debug_SysPrint("User Key is Pressed...\r\n");
    #endif
}

/**************************************************************************************************
**  函数名称:  UserKeyReleaseHdl
**  功能描述:  用户按键松开处理函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
static void UserKeyReleaseHdl(INPUT_PORT_E port, INPUT_TRIG_E trigmode)
{
    #if DEBUG_COMM > 0
    Debug_SysPrint("User Key is Released...\r\n");
    #endif
}

/**************************************************************************************************
**  函数名称:  APP_UsrTestInit
**  功能描述:  模块初始化函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void APP_UsrTestInit(void)
{
    OSL_MemManRegA(MemAllcHdl);
    OSL_MemManRegF(MemFreeHdl);
    
    s_testtmr_1t = OSL_CreateTimer(TestTmrProc_1t);
    OSL_StartTimer(s_testtmr_1t, PERIOD_TEST_1T);
    
    s_testtmr_1s = OSL_CreateTimer(TestTmrProc_1s);
    OSL_StartTimer(s_testtmr_1s, PERIOD_TEST_1S);
    
    s_testtmr_1m = OSL_CreateTimer(TestTmrProc_1m);
    OSL_StartTimer(s_testtmr_1m, PERIOD_TEST_1M);
    
    DAL_INPUT_InstallTriggerProc(INPUT_USRKEY, INPUT_TRIG_POSITIVE, UserKeyPressedHdl);
    DAL_INPUT_InstallTriggerProc(INPUT_USRKEY, INPUT_TRIG_NEGATIVE, UserKeyReleaseHdl);
}


