/**************************************************************************************************
**
**  文件名称:  usbd_bsp.h
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2019
**  文件描述:  USB-DEV库板级驱动
**  ===============================================================================================
**  创建信息:  | 2019-3-3 | LEON | 创建本模块
**  ===============================================================================================
**  修改信息:  单击此处添加....
**************************************************************************************************/
#ifndef USBD_BSP_H
#define USBD_BSP_H

#include "usb_core.h"
#include "usb_conf.h"

#ifdef USB_DEV

void USBD_BSP_Initiate(void);

void USB_OTG_BSP_Init(USB_OTG_CORE_HANDLE *pdev);
void USB_OTG_BSP_EnableInterrupt(USB_OTG_CORE_HANDLE *pdev);

void USB_OTG_BSP_DriveVBUS(USB_OTG_CORE_HANDLE *pdev, uint8_t state);
void USB_OTG_BSP_ConfigVBUS(USB_OTG_CORE_HANDLE *pdev);

void USB_OTG_BSP_uDelay(const uint32_t usec);
void USB_OTG_BSP_mDelay(const uint32_t msec);

void USB_SendData(uint8_t *dptr, uint32_t dlen);

#endif

#endif


