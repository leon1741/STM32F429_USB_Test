﻿/**************************************************************************************************
**
**  文件名称:  usbh_bsp.c
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2018
**  文件描述:  USB-HOST库板级驱动
**  ===============================================================================================
**  创建信息:  | 2018-6-11 | LEON | 创建本模块
**  ===============================================================================================
**  修改信息:  单击此处添加....
**************************************************************************************************/
#include "usb_include.h"
#include "usbh_bsp.h"
#include "usbh_usr.h"
#include "usbh_msc_core.h"
#include "usb_hcd_int.h"

#ifdef USB_HOST

/*************************************************************************************************/
//                           模块宏定义
/*************************************************************************************************/
#define HOST_POWERSW_PORT    GPIOC
#define HOST_POWERSW_VBUS    GPIO_Pin_4

/*************************************************************************************************/
//                           静态变量定义
/*************************************************************************************************/
static USB_OTG_CORE_HANDLE   USB_OTG_Dev;
static USBH_HOST             USB_HostDev;

/**************************************************************************************************
**  函数名称:  USBH_BSP_Initiate
**  功能描述:  USB板级驱动初始化
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USBH_BSP_Initiate(void)
{
    USBH_Init(&USB_OTG_Dev, USB_OTG_HS_CORE_ID, &USB_HostDev, &USBH_MSC_cb, &USR_Callbacks);
}

/**************************************************************************************************
**  函数名称:  USBH_BSP_MsgHandle
**  功能描述:  USB消息处理函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USBH_BSP_MsgHandle(void)
{
    USBH_Process(&USB_OTG_Dev, &USB_HostDev);
}

/**************************************************************************************************
**  函数名称:  USBH_BSP_GetOTGDev
**  功能描述:  获取USB OTG设备符
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
USB_OTG_CORE_HANDLE *USBH_BSP_GetOTGDev(void)
{
    return &USB_OTG_Dev;
}

/**************************************************************************************************
**  函数名称:  USBH_BSP_GetHostDev
**  功能描述:  获取USB OTG设备符
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
USBH_HOST *USBH_BSP_GetHostDev(void)
{
    return &USB_HostDev;
}

/**************************************************************************************************
**  函数名称:  USB_OTG_BSP_Init
**  功能描述:  初始化函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_OTG_BSP_Init(USB_OTG_CORE_HANDLE *pdev)
{
    GPIO_InitTypeDef GPIO_InitStructure;    

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_14 | GPIO_Pin_15;

    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOB, &GPIO_InitStructure);  

    GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_OTG2_FS); 
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_OTG2_FS);

    GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOB, &GPIO_InitStructure);  
    
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_OTG_HS, ENABLE);
}

/**************************************************************************************************
**  函数名称:  OTG_HS_IRQHandler
**  功能描述:  OTG中断处理函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void OTG_HS_IRQHandler(void)
{
    USBH_OTG_ISR_Handler(&USB_OTG_Dev);
}

/**************************************************************************************************
**  函数名称:  USB_OTG_BSP_EnableInterrupt
**  功能描述:  使能全局中断
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_OTG_BSP_EnableInterrupt(USB_OTG_CORE_HANDLE *pdev)
{
    NVIC_InitTypeDef NVIC_InitStructure; 
    
    NVIC_InitStructure.NVIC_IRQChannel = OTG_HS_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    
    NVIC_Init(&NVIC_InitStructure);
}

/**************************************************************************************************
**  函数名称:  USB_OTG_BSP_DriveVBUS
**  功能描述:  VBUS传输
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_OTG_BSP_DriveVBUS(USB_OTG_CORE_HANDLE *pdev, uint8_t state)
{
    if (0 == state) {
        GPIO_SetBits(HOST_POWERSW_PORT, HOST_POWERSW_VBUS);
    } else {
        GPIO_ResetBits(HOST_POWERSW_PORT, HOST_POWERSW_VBUS);
    }
}

/**************************************************************************************************
**  函数名称:  USB_OTG_BSP_ConfigVBUS
**  功能描述:  VBUS及过流配置
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_OTG_BSP_ConfigVBUS(USB_OTG_CORE_HANDLE *pdev)
{
    GPIO_InitTypeDef GPIO_InitStructure; 
  
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH , ENABLE);  

    GPIO_InitStructure.GPIO_Pin   = HOST_POWERSW_VBUS;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL ;
    GPIO_Init(HOST_POWERSW_PORT, &GPIO_InitStructure);

    GPIO_SetBits(HOST_POWERSW_PORT, HOST_POWERSW_VBUS);

    USB_OTG_BSP_mDelay(200);
}

/**************************************************************************************************
**  函数名称:  USB_OTG_BSP_uDelay
**  功能描述:  微秒延时函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_OTG_BSP_uDelay(const uint32_t usec)
{
    __IO uint32_t count = 0;
    const uint32_t utime = (120 * usec / 7);
    
    do {
        if (++count > utime) {
            return;
        }
    }
    while (1);
}

/**************************************************************************************************
**  函数名称:  USB_OTG_BSP_mDelay
**  功能描述:  毫秒延时函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_OTG_BSP_mDelay(const uint32_t msec)
{
    USB_OTG_BSP_uDelay(msec * 1000);
}

#endif


