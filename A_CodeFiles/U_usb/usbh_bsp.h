﻿/**************************************************************************************************
**
**  文件名称:  usbh_bsp.h
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2018
**  文件描述:  USB-HOST库板级驱动
**  ===============================================================================================
**  创建信息:  | 2018-6-11 | LEON | 创建本模块
**  ===============================================================================================
**  修改信息:  单击此处添加....
**************************************************************************************************/
#ifndef USBH_BSP_H
#define USBH_BSP_H

#include "usb_core.h"
#include "usbh_core.h"

#ifdef USB_HOST

void USBH_BSP_Initiate(void);
void USBH_BSP_MsgHandle(void);

void USB_OTG_BSP_Init(USB_OTG_CORE_HANDLE *pdev);
void USB_OTG_BSP_EnableInterrupt(USB_OTG_CORE_HANDLE *pdev);
void USB_OTG_BSP_uDelay(const uint32_t usec);
void USB_OTG_BSP_mDelay(const uint32_t msec);

USB_OTG_CORE_HANDLE *USBH_BSP_GetOTGDev(void);
USBH_HOST *USBH_BSP_GetHostDev(void);

#ifdef USE_HOST_MODE
void USB_OTG_BSP_ConfigVBUS(USB_OTG_CORE_HANDLE *pdev);
void USB_OTG_BSP_DriveVBUS(USB_OTG_CORE_HANDLE *pdev, uint8_t state);
#endif

#endif

#endif


