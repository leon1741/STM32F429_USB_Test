/**************************************************************************************************
**
**  文件名称:  usbd_usr.h
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2019
**  文件描述:  USB-DEV模式下的用户服务模块
**  ===============================================================================================
**  创建信息:  | 2019-3-3 | LEON | 创建本模块
**  ===============================================================================================
**  修改信息:  单击此处添加....
**************************************************************************************************/
#ifndef USBD_USR_H
#define USBD_USR_H

#include "usbd_def.h"
#include "usbd_core.h"

#ifdef USB_DEV

extern USBD_Usr_cb_TypeDef USR_Callbacks;

/*************************************************************************************************/
//                           USB设备状态枚举
/*************************************************************************************************/
typedef enum {
    USBD_STAT_INIT,                                                            /* 初始化 */
    USBD_STAT_DEINIT,                                                          /* 去初始化 */
    USBD_STAT_UNDEFINED
} USBD_STAT_E;


void USBD_USR_Init(void);
void USBD_USR_DeviceReset(uint8_t speed);
void USBD_USR_DeviceConfigured (void);
void USBD_USR_DeviceConnected (void);
void USBD_USR_DeviceDisconnected (void);
void USBD_USR_DeviceSuspended(void);
void USBD_USR_DeviceResumed(void);

void USBD_USR_Initiate(void);

#endif

#endif



