/**************************************************************************************************
**
**  文件名称:  usbd_bsp.c
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2019
**  文件描述:  USB-DEV库板级驱动
**  ===============================================================================================
**  创建信息:  | 2019-3-3 | LEON | 创建本模块
**  ===============================================================================================
**  修改信息:  单击此处添加....
**************************************************************************************************/
#include "usb_bsp.h"
#include "usb_dcd_int.h"
#include "usbd_core.h"
#include "usbd_conf.h"
#include "usbd_desc.h"
#include "usbd_usr.h"
#include "usbd_hid_core.h"

#ifdef USB_DEV

static USB_OTG_CORE_HANDLE  USB_OTG_dev;

/**************************************************************************************************
**  函数名称:  USBD_BSP_Initiate
**  功能描述:  USB板级驱动初始化
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USBD_BSP_Initiate(void)
{
    USBD_Init(&USB_OTG_dev, USB_OTG_HS_CORE_ID, &USR_desc, &USBD_HID_cb, &USR_Callbacks);
}

/**************************************************************************************************
**  函数名称:  USB_OTG_BSP_Init
**  功能描述:  初始化函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_OTG_BSP_Init(USB_OTG_CORE_HANDLE *pdev)
{
    GPIO_InitTypeDef GPIO_InitStructure;    

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB , ENABLE) ;
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_OTG_HS, ENABLE) ;

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;	
    GPIO_Init(GPIOB, &GPIO_InitStructure);  

    GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_OTG_HS_FS); 
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_OTG_HS_FS);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOB, &GPIO_InitStructure);  
}

/**************************************************************************************************
**  函数名称:  USB_OTG_BSP_EnableInterrupt
**  功能描述:  使能全局中断
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_OTG_BSP_EnableInterrupt(USB_OTG_CORE_HANDLE *pdev)
{
    NVIC_InitTypeDef NVIC_InitStructure; 

    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
    
    NVIC_InitStructure.NVIC_IRQChannel = OTG_HS_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/**************************************************************************************************
**  函数名称:  OTG_HS_IRQHandler
**  功能描述:  OTG中断处理函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void OTG_HS_IRQHandler(void)
{
    USBD_OTG_ISR_Handler(&USB_OTG_dev);
}

/**************************************************************************************************
**  函数名称:  USB_OTG_BSP_DriveVBUS
**  功能描述:  VBUS传输
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_OTG_BSP_DriveVBUS(USB_OTG_CORE_HANDLE *pdev, uint8_t state)
{
    ;
}

/**************************************************************************************************
**  函数名称:  USB_OTG_BSP_ConfigVBUS
**  功能描述:  VBUS及过流配置
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_OTG_BSP_ConfigVBUS(USB_OTG_CORE_HANDLE *pdev)
{
    ;
}

/**************************************************************************************************
**  函数名称:  USB_OTG_BSP_uDelay
**  功能描述:  微秒延时函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_OTG_BSP_uDelay(const uint32_t usec)
{
    uint32_t count = 0;
    const uint32_t utime = (120 * usec / 7);
    
    do {
        if (++count > utime) {
            return;
        }
    }
    while (1);
}

/**************************************************************************************************
**  函数名称:  USB_OTG_BSP_mDelay
**  功能描述:  毫秒延时函数
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_OTG_BSP_mDelay(const uint32_t msec)
{
    USB_OTG_BSP_uDelay(msec * 1000);   
}

/**************************************************************************************************
**  函数名称:  USB_SendData
**  功能描述:  发送USB数据
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void USB_SendData(uint8_t *dptr, uint32_t dlen)
{
    uint8_t HID_Buffer[4];
    
    HID_Buffer[0] = 0;
    HID_Buffer[1] = 0;
    HID_Buffer[2] = 0;
    HID_Buffer[3] = dlen;
    
    USBD_HID_SendReport(&USB_OTG_dev, HID_Buffer, 4);
}


#endif


