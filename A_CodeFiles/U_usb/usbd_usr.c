/**************************************************************************************************
**
**  文件名称:  usbd_usr.c
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2019
**  文件描述:  USB-DEV模式下的用户服务模块
**  ===============================================================================================
**  创建信息:  | 2019-3-3 | LEON | 创建本模块
**  ===============================================================================================
**  修改信息:  单击此处添加....
**************************************************************************************************/
#include "usb_conf.h"
#include "usbd_usr.h"
#include "usbd_ioreq.h"

#ifdef USB_DEV

USBD_Usr_cb_TypeDef USR_Callbacks =
{
    USBD_USR_Init,
    USBD_USR_DeviceReset,
    USBD_USR_DeviceConfigured,
    USBD_USR_DeviceSuspended,
    USBD_USR_DeviceResumed,
    USBD_USR_DeviceConnected,
    USBD_USR_DeviceDisconnected,  
};

void USBD_USR_Init(void)
{  

}

void USBD_USR_DeviceReset(uint8_t speed)
{

}

void USBD_USR_DeviceConfigured (void)
{

}

void USBD_USR_DeviceConnected (void)
{

}

void USBD_USR_DeviceDisconnected (void)
{

}

void USBD_USR_DeviceSuspended(void)
{
    ;
}

void USBD_USR_DeviceResumed(void)
{
    ;
}

void USBD_USR_Initiate(void)
{
    ;
}

#endif


