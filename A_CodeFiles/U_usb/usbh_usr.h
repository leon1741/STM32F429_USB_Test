/**************************************************************************************************
**
**  文件名称:  usbh_usr.c
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2019
**  文件描述:  USB HOST模式下的用户服务模块
**  ===============================================================================================
**  创建信息:  | 2019-3-3 | LEON | 创建本模块
**  ===============================================================================================
**  修改信息:  单击此处添加....
**************************************************************************************************/
#ifndef USBH_USR_H
#define USBH_USR_H

#include "usbh_def.h"
#include "usbh_core.h"

#ifdef USB_HOST

extern USBH_Usr_cb_TypeDef USR_Callbacks;

/*************************************************************************************************/
//                           USB设备状态枚举
/*************************************************************************************************/
typedef enum {
    USBH_STAT_INIT,                                                            /* 初始化 */
    USBH_STAT_DEINIT,                                                          /* 去初始化 */
    USBH_STAT_ATTACHED,                                                        /* 设备已检测到 */
    USBH_STAT_RESETDEVICE,                                                     /* 设备复位 */
    USBH_STAT_DISCONNECTED,                                                    /* 设备断开 */
    USBH_STAT_OVERCURRENT,                                                     /* 设备过流 */
    USBH_STAT_ENUMERATIONDONE,                                                 /* 设备枚举完毕 */
    USBH_STAT_MSC_APPLICATION,
    USBH_STAT_NOTSUPPORTED,                                                    /* 设备不支持 */
    USBH_STAT_UNRECOVEREDERROR,                                                /* 设备异常 */
    USBH_STAT_UNDEFINED
} USBH_STAT_E;

void USBH_USR_Init(void);
void USBH_USR_DeInit(void);
void USBH_USR_DeviceAttached(void);
void USBH_USR_ResetDevice(void);
void USBH_USR_DeviceDisconnected(void);
void USBH_USR_OverCurrentDetected(void);
void USBH_USR_DeviceSpeedDetected(uint8_t DeviceSpeed);
void USBH_USR_Device_DescAvailable(void *DeviceDesc);
void USBH_USR_DeviceAddressAssigned(void);
void USBH_USR_Configuration_DescAvailable(USBH_CfgDesc_TypeDef * cfgDesc, USBH_InterfaceDesc_TypeDef *itfDesc, USBH_EpDesc_TypeDef *epDesc);
void USBH_USR_Manufacturer_String(void *ManufacturerString);
void USBH_USR_Product_String(void *ProductString);
void USBH_USR_SerialNum_String(void *SerialNumString);
void USBH_USR_EnumerationDone(void);
USBH_USR_Status USBH_USR_UserInput(void);
int USBH_USR_MSC_Application(void);
void USBH_USR_DeviceNotSupported(void);
void USBH_USR_UnrecoveredError(void);

void USBH_USR_Initiate(void);
void USBH_USR_RegStatHdl(void (*inform)(USBH_STAT_E));

#endif

#endif


