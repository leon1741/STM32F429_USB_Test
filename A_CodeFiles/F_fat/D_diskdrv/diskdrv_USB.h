/**************************************************************************************************
**
**  文件名称:  diskdrv_USB.h
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2018
**  文件描述:  USB磁盘驱动管理模块
**  ===============================================================================================
**  创建信息:  | 2018-7-20 | LEON | 创建本模块
**  ===============================================================================================
**  修改信息:  单击此处添加....
**************************************************************************************************/
#ifndef DISKDRV_USB_H
#define DISKDRV_USB_H

#include "integer.h"

DSTATUS USB_disk_initialize(void);
DSTATUS USB_disk_status(void);
DRESULT USB_disk_read(BYTE *buff, DWORD sector, UINT count);
DRESULT USB_disk_write(const BYTE *buff, DWORD sector, UINT count);
DRESULT USB_disk_ioctl(BYTE ctrl, void *buff);

#endif

