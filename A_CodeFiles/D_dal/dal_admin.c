/**************************************************************************************************
**                                                                                               **
**  文件名称:  dal_admin.c                                                                       **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  DAL层入口及管控模块                                                               **
**  ===========================================================================================  **
**  创建信息:  | 2017-6-30 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#include "dal_include.h"
#include "dal_input_drv.h"
#include "dal_led_man.h"
#include "dal_lcd_drv.h"
#include "dal_ver_man.h"

/**************************************************************************************************
**  函数名称:  DALAdmin_Initiate
**  功能描述:  DAL层管控模块初始化
**  输入参数:  无
**  输出参数:  无
**  返回参数:  无
**************************************************************************************************/
void DALAdmin_Initiate(void)
{
    #if EN_DEBUG > 0
    Debug_Initiate();
    Debug_SysPrint("\r\n");
    Debug_SysPrint("System Start...\r\n");
    Debug_SysPrint("System Version: %s @ %s @ %s %s\r\n", GetSoftVersion(), GetHardVersion(), GetCompileDate(), GetCompileTime());
    #endif
    
    DAL_LED_Init();
    DAL_INPUT_Init();
    DAL_LCD_Init();
}


