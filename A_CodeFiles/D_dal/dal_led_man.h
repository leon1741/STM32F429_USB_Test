/**************************************************************************************************
**                                                                                               **
**  文件名称:  dal_led_man.h                                                                     **
**  版权所有:  CopyRight @ Xiamen Yaxon NetWork CO.LTD. 2017                                     **
**  文件描述:  实现对LED闪灯的管理控制                                                           **
**  ===========================================================================================  **
**  创建信息:  | 2017-3-17 | LEON | 创建本模块                                                   **
**  ===========================================================================================  **
**  修改信息:  单击此处添加....                                                                  **
**************************************************************************************************/
#ifndef DAL_LED_MAN_H
#define DAL_LED_MAN_H

void DAL_LED_Init(void);
void DAL_LED_StartLimitFlash(LED_NAME_E name, INT16U cycle, INT8U time_high, INT8U time_low);
void DAL_LED_StopLimitFlash(LED_NAME_E name);
void DAL_LED_StartPermentFlash(LED_NAME_E name, INT8U time_high, INT8U time_low, INT8U cycle, INT8U dead_time);
void DAL_LED_StopPermentFlash(LED_NAME_E name);

#endif

